package tarefas;

import java.util.List;

public interface TaskDAO {

	public List<Task> findAll();

	public void save(final Task task);
	
	public void remove(Task task);

}
