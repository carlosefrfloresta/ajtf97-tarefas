package tarefas;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class TaskFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	public static void main(final String[] args) {
		new TaskFrame().setVisible(true);
	}

	private final JPanel frmButtons = new JPanel();

	private final JPanel frmTask = new JPanel();
	private final TaskModel taskModel = new TaskModel();
	private TaskTable tblTasks;

	final JTextField txfTitle = new JTextField(40);

	public TaskFrame() {
		this.setSize(600, 500);
		this.setLocationRelativeTo(null);
		this.add(this.buildFrmTask(), BorderLayout.NORTH);
		this.add(new JScrollPane(this.buildTblTasks()), BorderLayout.CENTER);
		this.add(this.buildFrmButtons(), BorderLayout.SOUTH);
	}

	private JPanel buildFrmButtons() {
		final JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent e) {
				final String title = TaskFrame.this.txfTitle.getText();
				final Task task = new Task(title);
				TaskFrame.this.taskModel.save(task);
				TaskFrame.this.tblTasks.load(TaskFrame.this.taskModel);
			}
		});
		this.frmButtons.add(btnAdd);

		return this.frmButtons;
	}

	private JPanel buildFrmTask() {
		final JLabel lblTitle = new JLabel("Title:");
		this.frmTask.add(lblTitle);
		this.frmTask.add(this.txfTitle);
		return this.frmTask;
	}

	private JTable buildTblTasks() {
		this.tblTasks = new TaskTable();
		return this.tblTasks;
	}
}
