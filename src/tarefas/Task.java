package tarefas;

public class Task {

	private final String title;

	public Task(final String title) {
		this.title = title;
	}

	public String getTitle() {
		return this.title;
	}
}
