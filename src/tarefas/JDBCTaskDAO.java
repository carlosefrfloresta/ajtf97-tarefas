package tarefas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import sun.applet.Main;

public class JDBCTaskDAO implements TaskDAO {

	public static void main(final String[] args) {
		JDBCTaskDAO jdbcTaskDAO = new JDBCTaskDAO();
		final List<Task> tasks = jdbcTaskDAO.findAll();
		System.out.printf("%s tarefas encontradas", tasks.size());
		jdbcTaskDAO.remove(tasks.get(0));
		
	}

	private final String pass = "aj";
	private final String url = "jdbc:mysql://172.16.200.232:3306/task_schema";

	private final String user = "aj";

	@Override
	public List<Task> findAll() {
		final List<Task> result = new LinkedList<>();
		final String sql = "select * from task";
		try (final Connection connection = DriverManager.getConnection(this.url, this.user, this.pass);
				final PreparedStatement preparedStatement = connection.prepareStatement(sql);
				final ResultSet query = preparedStatement.executeQuery()) {
			while (query.next()) {
				final String title = query.getString("title");
				final Task task = new Task(title);
				result.add(task);
			}

		} catch (final SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void save(final Task task) {
		final String sql = "insert into task(title) values (?)";
		try (final Connection connection = DriverManager.getConnection(this.url, this.user, this.pass);
				final PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, task.getTitle());
			preparedStatement.executeUpdate();

		} catch (final SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void remove(Task task){
		String sql = "DELETE from task WHERE title = ?";
		try(Connection connection = DriverManager.getConnection(this.url,this.user,this.pass);
				PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, task.getTitle());
			preparedStatement.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
			
		
	}

}
