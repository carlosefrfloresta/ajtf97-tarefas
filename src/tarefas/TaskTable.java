package tarefas;

import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class TaskTable extends JTable {

	private static final long serialVersionUID = 1L;
	private final DefaultTableModel tableModel = new DefaultTableModel();

	public TaskTable() {
		super();
		this.tableModel.setColumnIdentifiers(new String[] { "title" });
		this.setModel(this.tableModel);
	}

	public void load(final TaskModel taskModel) {
		this.tableModel.setNumRows(0);
		final List<Task> tasks = taskModel.findAll();
		for (final Task task : tasks) {
			this.tableModel.addRow(new Object[] { task.getTitle() });
		}

	}

}
