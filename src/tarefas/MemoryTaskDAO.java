package tarefas;

import java.util.LinkedList;
import java.util.List;

public class MemoryTaskDAO implements TaskDAO {

	private final List<Task> result = new LinkedList<>();

	@Override
	public List<Task> findAll() {
		return this.result;
	}

	@Override
	public void save(final Task task) {
		this.result.add(task);
	}

	@Override
	public void remove(Task task) {
		this.result.remove(task);
		
	}

}
