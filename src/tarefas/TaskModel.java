package tarefas;

import java.util.List;

public class TaskModel {

	private final TaskDAO taskDAO = new DAOFactory().getTaskDAO();

	public List<Task> findAll() {
		return this.taskDAO.findAll();
	}

	public void save(final Task task) {
		this.taskDAO.save(task);
	}
}
